@echo off
set /p input1=Export Start Commit ID:
set commitIdStart=%input1%
set /p input2=Export End Commit ID:
if [%input2%] == [] (set commitIdEnd=HEAD) else (set commitIdEnd=%input2%)
set /p input3=Export Desternation Folder name:
if [%input3%] == [] (set Folder=FileDiff) else (set Folder=%input3%)
echo Start ID = %commitIdStart%
echo End ID = %commitIdEnd%
echo Export Desternation Folder = %Folder%

for /f "tokens=2,*" %%i in ('reg query "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders" /v "Desktop"') do set "desktop_dir=%%j"
echo Desktop Path = %desktop_dir%

for /f "usebackq tokens=*" %%A in (`git diff-tree -r --no-commit-id --name-only --diff-filter=ACMRT %commitIdStart% %commitIdEnd%`) do echo FA|xcopy "%%~fA" "%desktop_dir%/%Folder%/Mod/%%A"

git checkout %commitIdEnd%

set /p DUMMY = Hit Enter To Continue...